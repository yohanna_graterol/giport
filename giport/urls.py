from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'giport.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #admin
    url(r'^admin/', include(admin.site.urls)),
    
    #accounts
    url(r'^$','registration.views.inicio',name='inicio'),
    url(r'^accounts/', include('registration.urls')),
    url(r'^home/','registration.views.home',name='home'),
     url(r'^cerrar/','registration.views.salir',name='salir'),
    #url(r'^cursos_informacion/(\d+)','users.views.cursos_informacion',name='cursos_informacion'),

    #tareas
    #proyectos
    #
)
